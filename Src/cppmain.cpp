/*
 * main.cpp
 *
 *  Created on: Jan 12, 2017
 *      Author: faruxx
 */


#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "console.h"
#include "Adafruit_FONA.h"
#include "usart.h"

extern osTimerId signalGeneratorHandle;


static char mainBuffer[255];

extern ConsoleSerial console;
Adafruit_FONA_3G fona;
int generate_signal=1;
static const uint8_t no_masters=1;
const char* accepted_numbers[no_masters]={"+901112223344"};

int check_master(char* buffer)
{
	for(int i=0;i<no_masters;i++)
	{
		if(strcmp(buffer,accepted_numbers[i])==0)
		{
			return i;
		}
	}
	return -1;
}


bool send_sms(char const*sendto,char const *message)
	{
	// send an SMS!
	        if (!fona.sendSMS(sendto, message)) {
	          console.println("Failed");
	        } else {
	          console.println("Sent!");
	        }

		}

void wait_for_network(void)
{
	//Wait until connecting to network
			while(1){
				int n =fona.getNetworkStatus();
				if(n==1)
				{
					//Connected to network
					HAL_GPIO_WritePin(LD6_GPIO_Port,LD6_Pin,GPIO_PIN_SET);
					break;
				}
				osDelay(1000);
				HAL_GPIO_WritePin(LD6_GPIO_Port,LD6_Pin,GPIO_PIN_RESET);
			}}


bool read_message(char const * message)
{
	if(strcmp(message,"TURNON")==0)
	{
		generate_signal=1;
		return true;
	}
	else if(strcmp(message,"TURNOFF")==0)
	{
		generate_signal=0;
		return true;
	}
	return false;
}


extern "C"
/* StartDefaultTask function */
void StartDefaultTask(void const * argument)

{	//Start device

	//This code will put UART into listening state, after each byte it will
	//renew itself so no need for further operation
	console.prepNextReceive();

	///Wait device communication initialization procedures
	if (! fona.begin(console)) {
		while (1);
	}

	//Get imei, if needed
	char imei[15] = {0}; // MUST use a 16 character buffer for IMEI!
	uint8_t imeiLen = fona.getIMEI(imei);
	//Get type
	uint8_t type = fona.type();


	///Se if connected to system (n==1). If we are connected then PIN is either
	//	already provided or PIN protection is removed from SIM
	uint8_t n = fona.getNetworkStatus();
	//If not registered, unlock sim
	if(n==0){
		//Unlock by sending PIN
		char PIN[5]="9944";
		if (! fona.unlockSIM(PIN)) {
			//@WARNING Wrong pin, or device is already running
			while(1);
		}
	}


	//Delete all available sms in simcard
	for(int i=0;i<100;i++)
	{
		fona.deleteSMS(i);
	}

	//signal will be toogled in each 125ms
	//	which will create a pulse in 250ms=(125ms HIGH + 125ms LOW)
	//	which means 4Hz (1000ms (millisecond in seconds) / 250ms  (pulse width)
	osTimerStart(signalGeneratorHandle,125);

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
  	///Hangs operations until device connects to Network
  	wait_for_network();

  	//Get Number of SMS
  	// read the number of SMS's!
  	int8_t smsnum = fona.getNumSMS();
  	if(smsnum>0)
  	{
  		/**@WARNING: if there are multiple SMS in SIM card and lets say you have removed the first one,
  		 * the next one does not become the first one. It keeps its id as 2 while number of SMS's reduce from 2 to 1.
  		 * Which means in SIM there is an SMS but we don't know its id. We can ask for all of them but it is costly
  		 * So we always remove all SMS in system after a command when available. This way a new coming SMS is going to have ID 1.
  		 * And we will only read first SMS in SIM.
  		 */
  		//Read SMS number 1
  		if (fona.getSMSSender(1, mainBuffer, 250))
  		{
  			///If the number is registered above in accepted_numbers
  			int master=check_master(mainBuffer);
  			if(master!=-1)
  			{
  				//Read SMS with id 1, max length 250
  				uint16_t smslen;
  				if(fona.readSMS(1,mainBuffer,250,&smslen))
  				{

  					if(!read_message(mainBuffer))
  					{
  						///If command is not found then 'read_message' will return false, then we will
  						///send another message back saying the command is failed. If you don't have SMS
  						///package in your Phone Service then remove these
  						fona.sendSMS(accepted_numbers[master], "FAILED!");
  					}
  					else
  					{
  						///If successfull return another sms sayin OK
  						fona.sendSMS(accepted_numbers[master], "OK!");
  					}
  				}
  				else
  				{
  					///If could not read the sms, there is a system failure
  					fona.sendSMS(accepted_numbers[master], "FAILED!");
  				}
  			}
  			//Delete all available sms in simcard
  			for(int i=0;i<100;i++)
  			{
  				fona.deleteSMS(i);
  			}
			}
  		else
  		{

  		}

  	}

    osDelay(1000);
  }
  /* USER CODE END StartDefaultTask */
}


extern "C"
void signalGenerator_cb(void)
{
//Timer callback function
	if(generate_signal==1){
		HAL_GPIO_TogglePin(SIGNAL_GPIO_Port,SIGNAL_Pin);
		HAL_GPIO_TogglePin(LD3_GPIO_Port,LD3_Pin);
	}
	else
	{
		HAL_GPIO_WritePin(SIGNAL_GPIO_Port,SIGNAL_Pin,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LD3_GPIO_Port,LD3_Pin,GPIO_PIN_RESET);
	}
}


