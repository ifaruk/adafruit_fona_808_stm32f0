/*
 * ConsoleSerial.cpp
 *
 *  Created on: Jan 12, 2017
 *      Author: faruxx
 */

#include "console.h"
#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_uart_ex.h"

extern UART_HandleTypeDef huart1;

ConsoleSerial console;

ConsoleSerial::ConsoleSerial() {
	myBuffer=circular_buffer_create(100);
//	bufM=osMutexCreate(NULL);
}



ConsoleSerial::~ConsoleSerial(void) {
	circular_buffer_destroy(&myBuffer);
	// TODO Auto-generated destructor stub
}

void
ConsoleSerial::prepNextReceive(void)
{
	huart1.pRxBuffPtr = &data;
	huart1.RxXferSize = 1;
	huart1.RxXferCount = 1;

	/* Computation of UART mask to apply to RDR register */
	UART_MASK_COMPUTATION(&huart1);

	huart1.ErrorCode = HAL_UART_ERROR_NONE;
	huart1.RxState = HAL_UART_STATE_BUSY_RX;

	/* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
	SET_BIT(huart1.Instance->CR3, USART_CR3_EIE);

	/* Enable the UART Parity Error and Data Register not empty Interrupts */
	SET_BIT(huart1.Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);

}


size_t ConsoleSerial::write(uint8_t ch)
{
	if(HAL_OK == HAL_UART_Transmit(&huart1,&ch,1,10))
	{
		return 1;
	}
	return 0;
}



extern "C"
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	console.write_buffer(console.data);
	console.prepNextReceive();

}
