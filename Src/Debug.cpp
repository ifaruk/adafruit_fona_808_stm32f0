/*
 * debug.cpp
 *
 *  Created on: 15 Ara 2014
 *      Author: ifyalciner
 */

//@formatter:off
#include <cstdarg>
#include <cstdio>
 

//#include "board_usb.h"

#define DEBUG_BUFFER_STRING_LEN 200

void
debug_printf(const char* format,
             ...)
{
	using namespace std;
	char    buffer[DEBUG_BUFFER_STRING_LEN];
	va_list args;
	va_start(args,
	         format);
	vsprintf(buffer,
	         format,
	         args);
	va_end(args);
	/**@warning mostly size of buffer is smaller than systemEnvelopeContentLengthLimit, but when the message send
	 * and got free it will free all size (=systemEnvelopeContentLengthLimit), but during sending it will only send
	 * sizeof(buffer) amount
	 */
	//board_usb_puts(buffer);
}

int
debug_scanf(char* str,
            int len)
{
	return 0; //board_usb_gets(str,len);
}

void
debug_wait_connection(void)
{
	char test;
	//	while(board_usb_getchr( &test) == -1)
	//		;
}

void
debug_wait_signal(void)
{
	char test;
	//	while(board_usb_getchr( &test) != 1)
	//		;
}
//@formatter:on
