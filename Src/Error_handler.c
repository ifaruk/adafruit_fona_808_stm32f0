/*
 * FECS_ErrorHandler.c
 *
 *  Created on: May 8, 2013
 *      Author: ifyalciner
 */

#include "Error_handler.h"

void
halt_system_bare(void)
{

	__asm__("BKPT");

	while(1)
	{
	}
}

void
halt_system_main(unsigned long* hardfault_args)
{
	volatile unsigned long stacked_r0;
	volatile unsigned long stacked_r1;
	volatile unsigned long stacked_r2;
	volatile unsigned long stacked_r3;
	volatile unsigned long stacked_r12;
	volatile unsigned long stacked_lr;
	volatile unsigned long stacked_pc;
	volatile unsigned long stacked_psr;
	volatile unsigned long _CFSR;
	volatile unsigned long _HFSR;
	volatile unsigned long _DFSR;
	volatile unsigned long _AFSR;
	volatile unsigned long _BFAR;
	volatile unsigned long _MMAR;

	stacked_r0  = ((unsigned long)hardfault_args[0]);
	stacked_r1  = ((unsigned long)hardfault_args[1]);
	stacked_r2  = ((unsigned long)hardfault_args[2]);
	stacked_r3  = ((unsigned long)hardfault_args[3]);
	stacked_r12 = ((unsigned long)hardfault_args[4]);
	stacked_lr  = ((unsigned long)hardfault_args[5]);
	stacked_pc  = ((unsigned long)hardfault_args[6]);
	stacked_psr = ((unsigned long)hardfault_args[7]);

	/* Configurable Fault Status Register */
	/* Consists of MMSR, BFSR and UFSR */
	_CFSR = (* ((volatile unsigned long*)(0xE000ED28)));

	/* Hard Fault Status Register */
	_HFSR = (* ((volatile unsigned long*)(0xE000ED2C)));

	/* Debug Fault Status Register */
	_DFSR = (* ((volatile unsigned long*)(0xE000ED30)));

	/* Auxiliary Fault Status Register */
	_AFSR = (* ((volatile unsigned long*)(0xE000ED3C)));

	/* Read the Fault Address Registers. These may not contain valid values.
	 * Check BFARVALID/MMARVALID to see if they are valid values
	 * MemManage Fault Address Register
	 */
	_MMAR = (* ((volatile unsigned long*)(0xE000ED34)));
	/* Bus Fault Address Register */
	_BFAR = (* ((volatile unsigned long*)(0xE000ED38)));

	__asm("BKPT #0\n"); /* cause the debugger to stop */
}


__attribute__((naked)) void
halt_system(void)
{
	__asm volatile (
	" movs r0,#4      \n"  /* load bit mask into R0 */
		" movs r1, lr     \n"  /* load link register into R1 */
		" tst r0, r1      \n"  /* compare with bitmask */
		" beq _MSP        \n"  /* if bitmask is set: stack pointer is in PSP. Otherwise in MSP */
		" mrs r0, psp     \n"  /* otherwise: stack pointer is in PSP */
		" b _GetPC        \n"  /* go to part which loads the PC */
		"_MSP:            \n"  /* stack pointer is in MSP register */
		" mrs r0, msp     \n"  /* load stack pointer into R0 */
		"_GetPC:          \n"  /* find out where the hard fault happened */
		" ldr r1,[r0,#20] \n"  /* load program counter into R1. R1 contains address of the next instruction where the hard fault happened */
		" b halt_system_main      \n"  /* decode more information. R0 contains pointer to stack frame */
	);
	//  halt_system_main(0); /* dummy call to suppress compiler warning */
}

///**
//  * @brief  This function handles NMI exception.
//  * @param  None
//  * @retval None
//  */
//__attribute__((naked)) void NMI_Handler(void)
//{
// halt_system();
//}
//
///**
//  * @brief  This function handles Hard Fault exception.
//  * @param  None
//  * @retval None
//  */
//__attribute__((naked)) void HardFault_Handler(void)
//{
//  /* Go to infinite loop when Hard Fault exception occurs */
// halt_system();
//}

