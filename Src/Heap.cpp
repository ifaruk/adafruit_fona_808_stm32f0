/*
 * Memory.c
 *
 *  Created on: May 18, 2013
 *      Author: ifyalciner
 */

///formatter:off
#include <FreeRTOS.h>
#include <new>
#include <stdlib.h>

// Define the 'new' operator for C++ to use the freeRTOS memory management
// functions. THIS IS NOT OPTIONAL!
//


extern "C"
{
	void* malloc(size_t s)
	{
		return pvPortMalloc(s);
	}

	void free(void* p)
	{
		vPortFree(p);
		p = NULL;
	}
}


void*
operator new(std::size_t size)
{
	return pvPortMalloc(size);
}

void*
operator new[](std::size_t size)
{
	return pvPortMalloc(size);
}


void*
operator new(std::size_t size,
             const std::nothrow_t& tag)
{
	return pvPortMalloc(size);
}

void*
operator new[](std::size_t size,
               const std::nothrow_t& tag)
{
	return pvPortMalloc(size);
}

//
// Define the 'delete' operator for C++ to use the freeRTOS memory management
// functions. THIS IS NOT OPTIONAL!
//
void
operator delete(void* p)
{
	vPortFree(p);
	p = NULL;
}

void
operator delete[](void* p)
{
	vPortFree(p);
	p = NULL;
}


void
operator delete(void* p,

                const std::nothrow_t& tag)
{
	vPortFree(p);
	p = NULL;
}

void
operator delete[](void* p,
                  const std::nothrow_t& tag)
{
	vPortFree(p);
	p = NULL;
}
///formatter:on

