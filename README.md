# Adafruit FONA808 with stm32f0

Current project is using stm32f072b micro-controller on __STM32F072B-DISCO__ development board.

It uses FreeRTOS and HAL library, firmware is generated with STMCubeMX. 
STMCubeMX configuration file is aldo present in project. It can be easily ported and be used as
a base for new projects.

Current example:

* Activates the SIM code by inserting PIN code.
* Waits for commands from *accepted_numbers*.
* It normally generates a low frequency signal with GPIO. 
* If TURNOFF command arrives by SMS from an *accepted_number* it cuts the signal.
* If TURNON command arrives by SMS from an *accepted_number* it restarts to generate signal.
* After each command if command is successful then replies with __OK__.

###ISSUES
#####UART Performance
HAL UART library has a terrible system for Interrupt Based communication. 
It is not totally their fault since their aim is to provide the best generic
library for most devices. But it performs poorly for many cases which I sought this
one is also one of them.

With minimal dependency increase to the library introduces in __console.cpp__ for always active
interrupt based listening of UART RX. It is totally transparent to user where read data is 
placed in a circular buffer. But still it may require a little work when porting to different devices.

#####MASTER-SLAVE communication
Circular buffer in __console.cpp__ is not mutually exclusive in read-write access, 
which is would be normally be a terrible mistake in an application with shared resources.
Even though UART is full-duplex communication system. SIM808 works in master-slave mode where
slave (SIM808) always waits until uController is done with command then answers so it runs half-duplex.
Though no simultaneous read-write access will be done on circular-buffer and protecting it with a
mutex will cost unnecessary computation. 