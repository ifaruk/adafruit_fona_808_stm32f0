/*
 * debug.h
 *
 *  Created on: 15 Ara 2014
 *      Author: ifyalciner
 */

#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <swift_config.h>

#if DEBUG_LEVEL > 0
#define DEBUG_PRINTF( ...) debug_printf( __VA_ARGS__)
#define DEBUG_SCANF(str,len) debug_scanf(str,len)
#define DEBUG_WAIT_CONNECTION() debug_wait_connection()
#define DEBUG_WAIT_SIGNAL() debug_wait_signal()

void
debug_printf( const char* format,
		...);

int
debug_scanf(char* str,int len);

void
debug_wait_connection(void);

void
debug_wait_signal(void);

#else

#define DEBUG_PRINTF(...)
#define DEBUG_SCANF(str, len)
#define DEBUG_WAIT_CONNECTION()
#define DEBUG_WAIT_SIGNAL()

#endif

#endif /* _DEBUG_H_ */
