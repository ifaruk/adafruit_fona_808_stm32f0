/*
 * FONAPlatStd.h -- standard AVR/Arduino platform.
 *
 * This is part of the library for the Adafruit FONA Cellular Module
 *
 * Designed specifically to work with the Adafruit FONA
 * ----> https://www.adafruit.com/products/1946
 * ----> https://www.adafruit.com/products/1963
 * ----> http://www.adafruit.com/products/2468
 * ----> http://www.adafruit.com/products/2542
 *
 * Adafruit invests time and resources providing this open source code,
 * please support Adafruit and open-source hardware by purchasing
 * products from Adafruit!
 *
 * Written by Pat Deegan, http://flyingcarsandstuff.com, for inclusion in
 * the Adafruit_FONA_Library and released under the
 * BSD license, all text above must be included in any redistribution.
 *
 *  Created on: Jan 16, 2016
 *      Author: Pat Deegan
 */


#ifndef ADAFRUIT_FONA_LIBRARY_SRC_INCLUDES_PLATFORM_FONAPLATSTD_H_
#define ADAFRUIT_FONA_LIBRARY_SRC_INCLUDES_PLATFORM_FONAPLATSTD_H_

#include "FONAConfig.h"
#include "Stream.h"

// a few typedefs to keep things portable
typedef	Stream 						FONAStreamType;
typedef const char *	FONAFlashStringPtr;

#define prog_char  					char
#define PSTR(S)					S

#endif /* ADAFRUIT_FONA_LIBRARY_SRC_INCLUDES_PLATFORM_FONAPLATSTD_H_ */
