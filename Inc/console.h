/*
 * console.h
 *
 *  Created on: Jan 12, 2017
 *      Author: faruxx
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_

#include "Stream.h"
#include "circular_buffer.h"

class ConsoleSerial: public Stream {
public:
	ConsoleSerial();
	virtual ~ConsoleSerial();

	inline
	int available(){
		return circular_buffer_size(myBuffer);
	}
	inline  int read(){
		return circular_buffer_read_byte(myBuffer);
	}
	inline int peek(){
		return circular_buffer_peek_byte(myBuffer);
	}
	inline void flush(){
		circular_buffer_clear(myBuffer);
	}

	size_t write(uint8_t);

	inline void
	write_buffer(uint8_t c){
		circular_buffer_write(myBuffer,&c,1);
	}

	void prepNextReceive(void);

	uint8_t data;
//	osMutexId bufM;
private:
	Circular_buffer myBuffer;




};




#endif /* CONSOLE_H_ */
