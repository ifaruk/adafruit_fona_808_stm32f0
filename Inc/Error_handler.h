/*
 * FECS_ErrorHandler.h
 *
 *  Created on: May 8, 2013
 *      Author: ifyalciner
 */

#ifndef _ERRORHANDLER_H_
#define _ERRORHANDLER_H_

#ifdef __cplusplus
extern "C" {
#endif

void
halt_system(void);

#ifdef __cplusplus
}
#endif

#endif /* FECS_ERRORHANDLER_H_ */
