/*
 * circular_buffer.h
 *
 *  Created on: 25 Ara 2014
 *      Author: ifyalciner
 */

#ifndef _CIRCULAR_BUFFER_H_
#define _CIRCULAR_BUFFER_H_

#include "bitwise_hacks.h"
#include <stdint.h>
#include <stdlib.h>

#ifdef  __cplusplus
extern "C"
{
#endif

typedef struct Circular_buffer_t
{
	volatile uint32_t wrIdx;
	volatile uint32_t rdIdx;
	uint32_t          size;
	uint8_t* data;
}* Circular_buffer;

Circular_buffer
circular_buffer_create(uint32_t size);

static inline void
circular_buffer_destroy(Circular_buffer* cb)
{
	free((* cb)->data);
	free((* cb));
	* cb = NULL;
}

static inline uint8_t
circular_buffer_empty(Circular_buffer cb)
{
	return (cb->wrIdx == cb->rdIdx) ? 1 : 0;
}

static inline uint8_t
circular_buffer_full(Circular_buffer cb)
{
	return (cb->rdIdx == ((cb->wrIdx + 1) % cb->size)) ? 1 : 0;
}

static inline uint8_t
circular_buffer_clear(Circular_buffer cb)
{
	return cb->wrIdx = cb->rdIdx;
}

static inline uint32_t
circular_buffer_size(Circular_buffer cb)
{
	return (cb->wrIdx >= cb->rdIdx) ? (cb->wrIdx - cb->rdIdx) : (cb->size - (cb->rdIdx - cb->wrIdx));
}

static inline uint32_t
circular_buffer_free_size(Circular_buffer cb)
{
	return cb->size - circular_buffer_size(cb) - 1;
}

uint8_t
circular_buffer_write(Circular_buffer cb,
                      uint8_t* data,
                      uint32_t size);

static inline uint8_t
circular_buffer_read_byte(Circular_buffer cb)
{
	if(!circular_buffer_empty(cb)){
		uint8_t val = cb->data[cb->rdIdx++];
		cb->rdIdx %= cb->size;
		return val;
	}
	return 0;
}

static inline uint8_t
circular_buffer_peek_byte(Circular_buffer cb)
{
	return cb->data[cb->rdIdx];
}

static inline uint32_t
circular_buffer_get_rewind_point(Circular_buffer cb)
{
	return cb->rdIdx;
}

static inline uint8_t
circular_buffer_rewind_read_to(Circular_buffer cb,
                               uint8_t rp)
{
	if(cb->size > rp)
	{
		cb->rdIdx = rp;
		return 1;//True
	}
	else
	{
		return 0;//False
	}
}

#ifdef  __cplusplus
}
#endif

#endif /* _CIRCULAR_BUFFER_H_ */
